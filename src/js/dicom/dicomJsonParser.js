/*
  DICOM module.
  @module dicom
*/
var _base, _base2;
var __hasProp = Object.prototype.hasOwnProperty, __extends = function(child, parent) {
  for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; }
  function ctor() { this.constructor = child; }
  ctor.prototype = parent.prototype;
  child.prototype = new ctor();
  child.__super__ = parent.prototype;
  return child;
};
window.dwv = window.dwv || {};
 window.dwv.dicom = window.dwv.dicom || {};
window.dwv.image = window.dwv.image || {};
window.dwv.dicom.DicomJsonParser = (function() {
  __extends(DicomJsonParser, window.dwv.dicom.DicomParser);
  function DicomJsonParser() {
    DicomJsonParser.__super__.constructor.apply(this, arguments);
  }
  DicomJsonParser.prototype.parse = function(buffer) {
    var bigEndian, buf, dataElement, decoded, decoder, element, isJpeg2000, isJpegBaseline, isJpegLossless, isRLE, name, syntax, valueRepresenation, _results;
    syntax = dwv.dicom.cleanString(buffer.x00020010.value);
    isJpegBaseline = dwv.dicom.isJpegBaselineTransferSyntax(syntax);
    isJpegLossless = dwv.dicom.isJpegLosslessTransferSyntax(syntax);
    isJpeg2000 = dwv.dicom.isJpeg2000TransferSyntax(syntax);
    isRLE = dwv.dicom.isRLETransferSyntax(syntax);
    bigEndian = syntax === "1.2.840.10008.1.2.2";
    _results = [];
    for (name in buffer) {
      element = buffer[name];
      _results.push(element.tag === "x7FE00010" ? (this.pixelBuffer = dwv.decodeDataToUint8Array(atob(element.value)), decoder = null, isJpegLossless ? (decoder = new jpeg.lossless.Decoder(this.pixelBuffer.buffer), decoded = decoder.decode(), this.pixelBuffer = new Uint16Array(decoded.buffer)) : isJpegBaseline ? (decoder = new JpegImage(), decoder.parse(this.pixelBuffer), this.pixelBuffer = decoder.getData(decoder.width, decoder.height)) : isJpeg2000 ? (decoder = new JpxImage(), decoder.parse(this.pixelBuffer), this.pixelBuffer = decoder.tiles[0].items) : isRLE ? (decoder = new daikon.RLE(), buf = decoder.decode(this.pixelBuffer.buffer, true, buffer.x00280010.value * buffer.x00280011.value), this.pixelBuffer = new Uint16Array(buf.buffer)) : (valueRepresenation = buffer.x7FE00010.vr, !(valueRepresenation === 'OB' || (buffer.x00280100 && buffer.x00280100.value === 8)) ? this.pixelBuffer = new Uint16Array(this.pixelBuffer.buffer) : void 0)) : (dataElement = {
        'tag': {
          name: element.tag,
          group: "0x" + (element.tag.substr(1, 4)),
          element: "0x" + (element.tag.substr(5, 4))
        },
        'vr': element.vr,
        'vl': element.length,
        'data': element.vr === 'DS' && element.value ? element.value.split("\\") : [element.value]
      }, this.appendDicomElement(dataElement)));
    }
    return _results;
  };
  return DicomJsonParser;
})();
dwv.io.Url.prototype.prepareTextResponse = function(responseText) {
  var json;
  json = JSON.parse(responseText);
  window.dicomResponse = json;
  return dwv.image.getDataFromDicomJson(json);
};
dwv.image.getDataFromDicomJson = function(json) {
  var dicomParser, view, viewFactory;
  dicomParser = new dwv.dicom.DicomJsonParser();
  dicomParser.parse(json);
  window.dicomParser = dicomParser;
  viewFactory = new dwv.image.ViewFactory();
  view = viewFactory.create(dicomParser.getDicomElements(), dicomParser.getPixelBuffer());
  return {
    "view": view,
    "info": dicomParser.getDicomElements().dumpToTable(),
    "dicomElements": dicomParser.getDicomElements()
  };
};
dwv.decodeDataToUint8Array = function(data) {
  var array, k, v, _len;
  array = new Uint8Array(data.length);
  for (k = 0, _len = data.length; k < _len; k++) {
    v = data[k];
    array[k] = data.charCodeAt(k);
  }
  return array;
};
dwv.decodeDataToUint16Array = function(data) {
  return new Uint16Array(dwv.decodeDataToUint8Array(data).buffer);
};
dwv.decodeDataToUint32Array = function(data) {
  return new Uint32Array(dwv.decodeDataToUint8Array(data).buffer);
};