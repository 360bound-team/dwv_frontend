/**
 * Layout module.
 * @module tool
 */
var dwv = dwv || {};
dwv.tool = dwv.tool || {};


dwv.tool.Layout = function(launcher) {
    this.launcher = launcher;
    this.gui = null;
};

dwv.tool.Layout.prototype.setup = function() {
    this.gui = new dwv.gui.Layout( this.launcher );
    this.gui.setup();
};

dwv.tool.Layout.prototype.init = function() {

};

dwv.tool.Layout.prototype.display = function(bool) {
    this.gui.display(bool);
};