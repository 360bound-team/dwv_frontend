/**
 * GUI module.
 * @module gui
 */
var dwv = dwv || {};

/**
 * Namespace for GUI functions.
 * @class gui
 * @namespace dwv
 * @static
 */
dwv.gui = dwv.gui || {};
dwv.gui.base = dwv.gui.base || {};

/**
 * Toolbox base gui.
 * @class App
 * @namespace dwv.gui.base
 * @constructor
 */
dwv.gui.base.AppControl = function (options)
{
    this.launcher = options.launcher;
    this.appGui = options.appGui;
    this.app = options.appGui.app;
    this.appGui.controlGui = this;
    this.el = null;
};

/**
 * Initialise the HTML.
 * @method initialise
 */
dwv.gui.base.AppControl.prototype.initialise = function(options){

    var container = options.container;
    this.el = dwv.html.createElement( 'div', "dwv-app-control app-control-" + this.appGui.getId() );
    container.appendChild(this.el);
    var self = this;
    this.el.addEventListener('dblclick', function(){ self.launcher.openApp(self.appGui) });

    var canvasWrapper = dwv.html.createElement( 'div', 'dwv-app-control-wrapper' );
    this.el.appendChild(canvasWrapper);

    var img = dwv.html.createElement( 'img', 'dwv-app-control-canvas' );
    img.src = dwv.image.getImageDataUrl(this.app.getView());
    canvasWrapper.appendChild(img);
};

/**
 * Activate the HTML.
 * @method activate
 * @param {Boolean} bool True to activate, false to hide.
 */
dwv.gui.base.AppControl.prototype.activate = function(bool){
    if ( bool ) {
        dwv.html.addClass(this.el, 'active');
    } else {
        dwv.html.removeClass(this.el, 'active');
    }
};

///**
// * Render current image
// * @method renderImage
// */
//dwv.gui.base.AppControl.prototype.renderImage = function()
//{
//    console.log( dwv.image.getImageDataUrl(this.app.getView()) );
//    var canvas = this.el.getElementsByClassName('app-control-canvas')[0];
//    var width = canvas.offsetWidth;
//    var height = canvas.offsetHeight;
//    canvas.width = width;
//    canvas.height = height;
//
//    dwv.image.drawViewToCanvas(this.app.getView(), canvas);
//};

/**
 * Display the HTML.
 * @method display
 * @param {Boolean} bool True to display, false to hide.
 */
dwv.gui.base.AppControl.prototype.display = function(bool)
{
    dwv.html.displayElement(this.el, bool);
};

/**
 * Actvate assocated app
 * @method acivate
 * @param {String} window place for app.
 */
dwv.gui.base.AppControl.prototype.open = function(win)
{
    this.appGui.openMe(win)
};