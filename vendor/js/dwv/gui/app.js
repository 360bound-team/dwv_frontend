/**
 * GUI module.
 * @module gui
 */
var dwv = dwv || {};

/**
 * Namespace for GUI functions.
 * @class gui
 * @namespace dwv
 * @static
 */
dwv.gui = dwv.gui || {};
dwv.gui.base = dwv.gui.base || {};

/**
 * Toolbox base gui.
 * @class App
 * @namespace dwv.gui.base
 * @constructor
 */
dwv.gui.base.App = function (options)
{
    this.app = options.app;
    this.launcher = options.launcher;
    this.appEl = dwv.html.createElement('div', 'dicom-app');
    this.appEl.id = this.generateId();
    this.appEl.addEventListener( 'click', this.activateMe.bind(this) );
    var layerEl = dwv.html.createElement('div', 'layerContainer');
    layerEl.appendChild( dwv.html.createElement('canvas', 'imageLayer') );
    layerEl.appendChild( dwv.html.createElement('div', 'drawDiv') );

    var infoLayerEl = dwv.html.createElement('div', 'infoLayer');
    infoLayerEl.appendChild( dwv.html.createElement('div', 'infotl') );
    infoLayerEl.appendChild( dwv.html.createElement('div', 'infotr') );
    infoLayerEl.appendChild( dwv.html.createElement('div', 'infobl') );

    var tmpEl =  dwv.html.createElement('div', 'infobr');
    tmpEl.appendChild( dwv.html.createElement('div', 'plot') );

    infoLayerEl.appendChild( tmpEl );
    this.appEl.appendChild( layerEl );
    this.appEl.appendChild( infoLayerEl );
    dwv.gui.refreshElement( this.appEl );
};

dwv.gui.base.App.prototype.getId = function()
{
    return this.appEl.id;
};

/**
* Display the App HTML.
* @method display
* @param {Boolean} bool True to display, false to hide.
*/
dwv.gui.base.App.prototype.display = function(bool)
{
    bool ? dwv.html.addClass(this.appEl, 'active') : dwv.html.removeClass(this.appEl, 'active');
    dwv.html.displayElement(this.appEl, bool);
};

dwv.gui.base.App.prototype.activate = function(bool)
{
    if ( bool ) {
        dwv.html.addClass( this.appEl, 'active' );
    } else {
        dwv.html.removeClass( this.appEl, 'active');
    }
};

dwv.gui.base.App.prototype.activateMe = function(){
    if (this.isActive()) return;
    this.launcher.activateApp(this);
};

dwv.gui.base.App.prototype.openMe = function(win){
    this.launcher.openApp(this, win);
};

dwv.gui.base.App.prototype.isActive = function(){
    return dwv.html.hasClass(this.appEl, 'active');
};

/**
 * Initialise the app HTML.
 * @method initialise
 */
dwv.gui.base.App.prototype.initialise = function (options)
{
    var container = options.container;
    delete options.container;
    container.appendChild(this.appEl);
    this.app.init( this.appOptions() );
};

dwv.gui.base.App.prototype.fitToParentSize = function () {
    this.app.fitToSize( {
        'width': this.appEl.offsetWidth,
        'height': this.appEl.offsetHeight
    });
};

/**
 * Options for App.
 * @method appOptions
 */
dwv.gui.base.App.prototype.appOptions = function ()
{
    return {
        containerDivId: this.appEl.id,
        fitToWindow: this.launcher.options.fitToWindow
    }
};

/**
 * Return ID for HTML.
 * @method initialise
 */
var k = 0;
dwv.gui.base.App.prototype.generateId = function ()
{
    k = k + 1;
    return "app-" + k;
};