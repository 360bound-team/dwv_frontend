/**
 * Info module.
 * @module info
 */
var dwv = dwv || {};
/**
 * Namespace for info functions.
 * @class info
 * @namespace dwv
 * @static
 */
dwv.info = dwv.info || {};

dwv.info.Info = function( app, dicomElements ) {
    this.app = app;
    this.element = app.getElement("infoLayer");
    this.dicomElements = dicomElements;
    this.leftTopInfo = null;
    this.rightTopInfo = null;
    this.leftBottomInfo = null;
    this.rightBottomInfo = null;
    this.plotInfo = null;
    this.onUpdate = this.update.bind(this)
};

dwv.info.Info.prototype.create = function(){

    var infotl = this.getElement("infotl");
    this.leftTopInfo = new dwv.info.TopLeftInfo(infotl, this.dicomElements, this.app);
    this.leftTopInfo.create();

    var infotr = this.getElement("infotr");
    this.rightTopInfo = new dwv.info.TopRightInfo(infotr, this.dicomElements, this.app);
    this.rightTopInfo.create();

    var infobr = this.getElement("infobr");
    this.rightBottomInfo = new dwv.info.MiniColourMap(infobr, this.app);
    this.rightBottomInfo.create();

    var plot = this.getElement("plot");
    this.plotInfo = new dwv.info.Plot(plot, this.app);
    this.plotInfo.create();
};

dwv.info.Info.prototype.getElement = function(selector){
    return this.element.getElementsByClassName(selector)[0];
};

dwv.info.Info.prototype.update = function(event) {
    //view.addEventListener("wlchange", infoLayer.update);
    //view.addEventListener("wlchange", miniColourMap.update);
    //view.addEventListener("wlchange", plotInfo.update);
    //view.addEventListener("colourchange", miniColourMap.update);
    //view.addEventListener("positionchange", positionInfo.update);
    switch (event.type) {
        case 'wlchange' :
            this.rightBottomInfo.update(event);
            this.plotInfo.update(event);
            break;
        case 'positionchange' :
            break;
        case 'colourchange' :
            this.rightBottomInfo.update(event);
            break;
        case 'slicechange' :
            this.leftTopInfo.updateSliceInfo();
            break;
        case 'appendlayer' :
            this.leftTopInfo.updateSliceInfo();
            break;
    }
};
//class dwv.info.Info

var getValue = function(key) {
    return this.dicomElements.getFromName(key) || ''
};

dwv.info.formatDate = function(date) {
    var dateParts = date.match(/(\d{4})(\d{2})(\d{2})/)
    if (!dateParts) return date;
    return dateParts.slice(1,4).join('-');
};

dwv.info.TopLeftInfo = function( div, dicomElements, app ) {
    this.dicomElements = dicomElements;
    this.element = div;
    this.app = app;
};

dwv.info.TopLeftInfo.prototype.create = function(){
    dwv.html.cleanNode(this.element);

    this.element.appendChild( dwv.html.createElement('p', 'slice-info', this.getTotalSlices() + '/' + this.getCurrentSlice()) );
    this.element.appendChild( dwv.html.createElement('p', null, 'Acc#: ' + this.getValue('AccessionNumber')) );
    this.element.appendChild( dwv.html.createElement('p', null, 'Study Date: ' + dwv.info.formatDate( this.getValue('StudyDate') )) );
    this.element.appendChild( dwv.html.createElement('p', null, 'Study: ' + this.getValue('StudyDescription')) );
    this.element.appendChild( dwv.html.createElement('p', null, 'Series: ' + this.getValue('SeriesDescription')) );
    this.element.appendChild( dwv.html.createElement('p', null, 'Se#: ' + this.getValue('SeriesNumber')) );
    this.element.appendChild( dwv.html.createElement('p', null, this.getValue('InstitutionName')) );
    //this.element.appendChild( dwv.html.createElement('p', null, 'Instance: ' + this.getValue('InstanceNumber')) );
    //el.getFromName('StudyDate')
    //el.getFromName('StudyTime')
    //el.getFromName('StudyDescription')
    //el.getFromName('SeriesDescription')
    //el.getFromName('SeriesNumber')
    //el.getFromName('InstanceNumber')
};

dwv.info.TopLeftInfo.prototype.updateSliceInfo = function() {
    sliceInfo = this.element.getElementsByClassName('slice-info')[0]
    if ( sliceInfo ) sliceInfo.textContent = this.getTotalSlices() + '/' + this.getCurrentSlice();
};

dwv.info.TopLeftInfo.prototype.getValue = function(key){
    return getValue.call(this, key)
};

dwv.info.TopLeftInfo.prototype.getTotalSlices = function(){
    return this.app.getTotalSlices();
};

dwv.info.TopLeftInfo.prototype.getCurrentSlice = function(){
    return this.app.getCurrentSlice() + 1
};
//class dwv.info.TopRightInfo

dwv.info.TopRightInfo = function( div, dicomElements, app ) {
    this.dicomElements = dicomElements;
    this.element = div;
    this.app = app;
};

dwv.info.TopRightInfo.prototype.create = function(){
    dwv.html.cleanNode( this.element );
    this.element.appendChild( dwv.html.createElement('p', null, 'Name: ' + this.getValue('PatientName')) );
    this.element.appendChild( dwv.html.createElement('p', null, 'PID: ' + this.getValue('PatientID')) );
    this.element.appendChild( dwv.html.createElement('p', null, 'Sex: ' + this.getValue('PatientSex') ) );
    this.element.appendChild( dwv.html.createElement('p', null, 'DOB: ' + dwv.info.formatDate(this.getValue('PatientBirthDate'))) );
};

dwv.info.TopRightInfo.prototype.getValue = function(key){
    return getValue.call(this, key)
};


/**
 * MiniColourMap info layer.
 * @class MiniColourMap
 * @namespace dwv.info
 * @constructor
 * @param {Object} div The HTML element to add colourMap info to.
 * @param {Object} app The associated application.
 */
dwv.info.MiniColourMap = function ( div, app ) {
    this.element = div;
    this.app = app;
};

/**
 * Create the mini colour map info div.
 * @method create
 */
dwv.info.MiniColourMap.prototype.create = function () {
    // clean div
    var elems = this.element.getElementsByClassName("colour-map-info");
    if ( elems.length !== 0 ) {
        dwv.html.removeNodes(elems);
    }
    // colour map
    var canvas = document.createElement("canvas");
    canvas.className = "colour-map-info";
    canvas.width = 98;
    canvas.height = 10;
    // add canvas to div
    this.element.appendChild(canvas);
};

/**
 * Update the mini colour map info div.
 * @method update
 * @param {Object} event The windowing change event containing the new values.
 * Warning: expects the mini colour map div to exist (use after createMiniColourMap).
 */
dwv.info.MiniColourMap.prototype.update = function (event) {
    var div = this.element;
    var app = this.app;

    var windowCenter = event.wc;
    var windowWidth = event.ww;

    var canvas = div.getElementsByClassName("colour-map-info")[0];
    var context = canvas.getContext('2d');

    // fill in the image data
    var colourMap = app.getViewController().getColourMap();
    var imageData = context.getImageData(0,0,canvas.width, canvas.height);

    var c = 0;
    var minInt = app.getImage().getRescaledDataRange().min;
    var range = app.getImage().getRescaledDataRange().max - minInt;
    var incrC = range / canvas.width;
    var y = 0;

    var yMax = 255;
    var yMin = 0;
    var xMin = windowCenter - 0.5 - (windowWidth-1) / 2;
    var xMax = windowCenter - 0.5 + (windowWidth-1) / 2;

    var index;
    for( var j=0; j<canvas.height; ++j ) {
        c = minInt;
        for( var i=0; i<canvas.width; ++i ) {
            if( c <= xMin ) {
                y = yMin;
            }
            else if( c > xMax ) {
                y = yMax;
            }
            else {
                y = ( (c - (windowCenter-0.5) ) / (windowWidth-1) + 0.5 ) *
                (yMax-yMin) + yMin;
                y = parseInt(y,10);
            }
            index = (i + j * canvas.width) * 4;
            imageData.data[index] = colourMap.red[y];
            imageData.data[index+1] = colourMap.green[y];
            imageData.data[index+2] = colourMap.blue[y];
            imageData.data[index+3] = 0xff;
            c += incrC;
        }
    }
    // put the image data in the context
    context.putImageData(imageData, 0, 0);
};
// class dwv.info.MiniColourMap


/**
 * Plot info layer.
 * @class Plot
 * @namespace dwv.info
 * @constructor
 * @param {Object} div The HTML element to add colourMap info to.
 * @param {Object} app The associated application.
 */
dwv.info.Plot = function (div, app) {
    this.element = div;
    this.app = app;
};

/**
 * Create the plot info.
 * @method create
 */
dwv.info.Plot.prototype.create = function() {
    // clean div
    dwv.html.cleanNode(this.element);
    // create
    $.plot(this.element, [ this.app.getImage().getHistogram() ], {
        "bars": { "show": true },
        "grid": { "backgroundcolor": null },
        "xaxis": { "show": true },
        "yaxis": { "show": false }
    });
};

/**
 * Update plot.
 * @method update
 * @param {Object} event The windowing change event containing the new values.
 * Warning: expects the plot to exist (use after createPlot).
 */
dwv.info.Plot.prototype.update = function (event) {
    var wc = event.wc;
    var ww = event.ww;

    var half = parseInt( (ww-1) / 2, 10 );
    var center = parseInt( (wc-0.5), 10 );
    var min = center - half;
    var max = center + half;

    var markings = [
        { "color": "#faa", "lineWidth": 1, "xaxis": { "from": min, "to": min } },
        { "color": "#aaf", "lineWidth": 1, "xaxis": { "from": max, "to": max } }
    ];

    $.plot(this.element, [ this.app.getImage().getHistogram() ], {
        "bars": { "show": true },
        "grid": { "markings": markings, "backgroundcolour": null },
        "xaxis": { "show": false },
        "yaxis": { "show": false }
    });
};

/**
 * WindowLevel info layer.
 * @class Windowing
 * @namespace dwv.info
 * @constructor
 * @param {Object} div The HTML element to add WindowLevel info to.
 */
dwv.info.Windowing = function ( div )
{
    /**
     * Create the windowing info div.
     * @method create
     */
    this.create = function ()
    {
        // clean div
        var elems = div.getElementsByClassName("wl-info");
        if ( elems.length !== 0 ) {
            dwv.html.removeNodes(elems);
        }
        // create windowing list
        var ul = document.createElement("ul");
        ul.className = "wl-info";
        // window center list item
        var liwc = document.createElement("li");
        liwc.className = "window-center";
        ul.appendChild(liwc);
        // window width list item
        var liww = document.createElement("li");
        liww.className = "window-width";
        ul.appendChild(liww);
        // add list to div
        div.appendChild(ul);
    };

    /**
     * Update the windowing info div.
     * @method update
     * @param {Object} event The windowing change event containing the new values as {wc,ww}.
     * Warning: expects the windowing info div to exist (use after create).
     */
    this.update = function (event)
    {
        // window center list item
        var liwc = div.getElementsByClassName("window-center")[0];
        dwv.html.cleanNode(liwc);
        liwc.appendChild(document.createTextNode("WindowCenter = "+event.wc));
        // window width list item
        var liww = div.getElementsByClassName("window-width")[0];
        dwv.html.cleanNode(liww);
        liww.appendChild(document.createTextNode("WindowWidth = "+event.ww));
    };

}; // class dwv.info.Windowing

/**
 * Position info layer.
 * @class Position
 * @namespace dwv.info
 * @constructor
 * @param {Object} div The HTML element to add Position info to.
 */
dwv.info.Position = function ( div )
{
    /**
     * Create the position info div.
     * @method create
     */
    this.create = function ()
    {
        // clean div
        var elems = div.getElementsByClassName("pos-info");
        if ( elems.length !== 0 ) {
            dwv.html.removeNodes(elems);
        }
        // position list
        var ul = document.createElement("ul");
        ul.className = "pos-info";
        // position
        var lipos = document.createElement("li");
        lipos.className = "position";
        ul.appendChild(lipos);
        // value
        var livalue = document.createElement("li");
        livalue.className = "value";
        ul.appendChild(livalue);
        // add list to div
        div.appendChild(ul);
    };

    /**
     * Update the position info div.
     * @method update
     * @param {Object} event The position change event containing the new values as {i,j,k}
     *  and optional 'value'.
     * Warning: expects the position info div to exist (use after create).
     */
    this.update = function (event)
    {
        // position list item
        var lipos = div.getElementsByClassName("position")[0];
        dwv.html.cleanNode(lipos);
        lipos.appendChild(document.createTextNode(
            "Pos = "+event.i+", "+event.j+", "+event.k));
        // value list item
        if( typeof(event.value) != "undefined" )
        {
            var livalue = div.getElementsByClassName("value")[0];
            dwv.html.cleanNode(livalue);
            livalue.appendChild(document.createTextNode("Value = "+event.value));
        }
    };
}; // class dwv.info.Position