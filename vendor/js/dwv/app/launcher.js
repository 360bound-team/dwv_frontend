// Main DWV namespace.
var dwv = dwv || {};

dwv.onSeriesAppCreate = function(appGui){};
dwv.onAppControlCreate = function(appControlGui){};
dwv.onApplicationStarted = function(launcher){};

dwv.Launcher = function (options)
{
    //Series data
    this.series = {};

    this.activeAppGui = null;

    this.options = options;

    this.toolbox = null;

    this.toolboxController = null;

    this.undoStack = null;

    this.tagsGui = null;

    this.waitingSeries = {}

    if (options) {
        this.init(options);
    }

};

/**
 * Initialise the HTML for the launcher.
 * @method init
 */
dwv.Launcher.prototype.init = function ( config ) {
    this.containerId = config.containerId;
    // tools
    if ( config.tools && config.tools.length !== 0 ) {
        // setup the tool list
        var toolList = {};
        for ( var t = 0; t < config.tools.length; ++t ) {
            switch( config.tools[t] ) {
                case "Window/Level":
                    toolList["Window/Level"] = new dwv.tool.WindowLevel(this);
                    break;
                case "Magnifier":
                    toolList["Magnifier"] = new dwv.tool.Magnifier(this);
                    break;
                case "Zoom/Pan":
                    toolList["Zoom/Pan"] = new dwv.tool.ZoomAndPan(this);
                    break;
                case "Scroll":
                    toolList.Scroll = new dwv.tool.Scroll(this);
                    break;
                case "Draw":
                    if ( config.shapes !== 0 ) {
                        // setup the shape list
                        var shapeList = {};
                        for ( var s = 0; s < config.shapes.length; ++s ) {
                            switch( config.shapes[s] ) {
                                case "Line":
                                    shapeList.Line = dwv.tool.LineFactory;
                                    break;
                                case "Protractor":
                                    shapeList.Protractor = dwv.tool.ProtractorFactory;
                                    break;
                                case "Rectangle":
                                    shapeList.Rectangle = dwv.tool.RectangleFactory;
                                    break;
                                case "Roi":
                                    shapeList.Roi = dwv.tool.RoiFactory;
                                    break;
                                case "Ellipse":
                                    shapeList.Ellipse = dwv.tool.EllipseFactory;
                                    break;
                            }
                        }
                        toolList.Draw = new dwv.tool.Draw(this, shapeList);
                        //toolList.Draw.addEventListener("draw-create", fireEvent);
                        //toolList.Draw.addEventListener("draw-change", fireEvent);
                        //toolList.Draw.addEventListener("draw-move", fireEvent);
                        //toolList.Draw.addEventListener("draw-delete", fireEvent);
                    }
                    break;
                case "Livewire":
                    toolList.Livewire = new dwv.tool.Livewire(this);
                    break;
                case "Filter":
                    if ( config.filters.length !== 0 ) {
                        // setup the filter list
                        var filterList = {};
                        for ( var f = 0; f < config.filters.length; ++f ) {
                            switch( config.filters[f] ) {
                                case "Threshold":
                                    filterList.Threshold = new dwv.tool.filter.Threshold(this);
                                    break;
                                case "Sharpen":
                                    filterList.Sharpen = new dwv.tool.filter.Sharpen(this);
                                    break;
                                case "Sobel":
                                    filterList.Sobel = new dwv.tool.filter.Sobel(this);
                                    break;
                            }
                        }
                        toolList.Filter = new dwv.tool.Filter(filterList, this);
                    }
                    break;
                case "Layout":
                    var Layout = new dwv.gui.Layout(this);
                    break;
                default:
                    throw new Error("Unknown tool: '" + config.tools[t] + "'");
            }
        }
        this.toolbox = new dwv.tool.Toolbox( toolList, this );
        this.toolboxController = new dwv.ToolboxController( this.toolbox );
        window.addEventListener("keydown", this.eventHandler.bind(this), true);
    }
    // gui
    if ( config.gui ) {
        this.bindEventsMethods();
        // tools
        if ( config.gui.indexOf("tool") !== -1 && this.toolbox) {
            this.toolbox.setup();
            Layout.setup();
        }
        // load
        if ( config.gui.indexOf("load") !== -1 ) {
            loadbox = new dwv.gui.Loadbox(this);
            loadbox.setup();
        }
        // undo
        if ( config.gui.indexOf("undo") !== -1 ) {
            this.undoStack = new dwv.tool.UndoStack(this);
            this.undoStack.setup();
        }
        // DICOM Tags
        if ( config.gui.indexOf("tags") !== -1 ) {
            this.tagsGui = new dwv.gui.DicomTags(this);
        }
        // version number
        if ( config.gui.indexOf("version") !== -1 ) {
            dwv.gui.appendVersionHtml(this.getVersion());
        }
        // help
        if ( config.gui.indexOf("help") !== -1 ) {
            var isMobile = true;
            if ( config.isMobile ) {
                isMobile = config.isMobile;
            }
            dwv.gui.appendHelpHtml( toolbox.getToolList(), isMobile, this );
        }
    }
    window.launcher = this;
    this.windowsManager = new dwv.WindowsManager(this);
    this.seriesSynchronizer = new dwv.SeriesSynchronizer();
    dwv.onApplicationStarted(this);
};

/**
 * Bind events methods to instanse.
 * @method bindEventsMethods
 */
dwv.Launcher.prototype.bindEventsMethods = function ()
{
    this.onChangeWindowLevelPreset = this.onChangeWindowLevelPreset.bind(this);
    this.onChangeColourMap = this.onChangeColourMap.bind(this);
    this.onChangeTool = this.onChangeTool.bind(this);
    this.onChangeShape = this.onChangeShape.bind(this);
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onChangeLineColour = this.onChangeLineColour.bind(this);
    this.onZoomReset = this.onZoomReset.bind(this);
    this.onRunFilter = this.onRunFilter.bind(this);
};

/**
 * Reset the launcher.
 * @method reset
 */
dwv.Launcher.prototype.reset = function ()
{
    if ( this.toolbox ) this.toolbox.reset();

    if ( this.undoStack ) {
        this.undoStack = new dwv.tool.UndoStack(this);
        this.undoStack.setup();
    }

    var appsBlock = this.getElement('app-container');
    //if ( appsBlock ) appsBlock.innerHTML = '';

    var appsControlsBlock = this.getElement('app-controls');
    if ( appsControlsBlock ) appsControlsBlock.innerHTML = '';

    this.activeAppGui = null;
    this.apps = {};
    this.series = {};
};

/**
 * Load a list of URLs.
 * @method loadURL
 * @param {Array} urls The list of urls to load.
 */
dwv.Launcher.prototype.loadURL = function(urls, reset)
{
    // clear variables
    if ( reset == undefined ){ reset = true }
    if ( reset ) { this.reset() };
    dwv.gui.updateProgress({lengthComputable: true, loaded: 0, total: urls.length});
    var urlIO = new dwv.io.Url();
    urlIO.onload = this.onDcmLoaded.bind(this);
    urlIO.onerror = this.handleError.bind(this);
    urlIO.load(urls);
};

/**
 * Load a list of image files.
 * @method loadImageFiles
 * @param {Array} files The list of image files to load.
 */
dwv.Launcher.prototype.loadImageFiles = function (files)
{
    this.reset();
    dwv.gui.updateProgress({lengthComputable: true, loaded: 0, total: files.length});
    var fileIO = new dwv.io.File();
    fileIO.onload = this.onDcmLoaded.bind(this);
    fileIO.onerror = this.handleError.bind(this);
    fileIO.load(files)
};

/**
 * Handle loaded DICOM data. To be called once the DICOM has been parsed.
 * @method onDcmLoaded
 * @param {Object} data The data to display.
 */
dwv.Launcher.prototype.onDcmLoaded = function(data)
{
    var dicomData = data.dicomElements;
    var seriesId = dicomData.getFromName('SeriesInstanceUID');

    if ( this.apps[seriesId] ) {
        this.addSeries(data);
    }
    else {
        this.initApp(data);
    }
};


/**
 * Initialize new Application. To be called once the new series DICOM has been parsed.
 * @method initApp
 * @param {Object} data The data to display.
 */
dwv.Launcher.prototype.initApp = function(data)
{
    var dicomData = data.dicomElements;
    var seriesId = dicomData.getFromName('SeriesInstanceUID');
    var app = new dwv.App(seriesId);
    var appGui = new dwv.gui.App({app: app, launcher: this});
    var appControl = new dwv.gui.AppControl({launcher: this, appGui: appGui});

    app.info = data.info;
    this.apps[seriesId] = appGui;
    appGui.initialise({ container: this.getElement('app-cache') });
    app.addLayer(data);
    appControl.initialise({ container: this.getElement('app-controls') });

    dwv.onSeriesAppCreate(appGui);
    dwv.onAppControlCreate(appControl);

    if (this.toolbox) {
        app.addLayerListeners( app.getImageLayer().getCanvas() );
        var toolbox = this.toolbox;
        app.getToolbox = function(){ return toolbox; }
    }

    if ( !this.activeAppGui ) {
        this.activeAppGui = appGui;
        console.log('open');
        this.openApp(appGui);
    }

    return appGui;
};

/**
 * Initialize series.
 * @method initializeSeries
 * @param {Object} The grouped by series uid data
 */
dwv.Launcher.prototype.initSeries = function(series) {
    var firstSlidesUrls = [];
    var seriesUrls;
    for (var seriesUID in series) {
        seriesUrls = series[seriesUID];
        firstSlidesUrls.push( seriesUrls.shift(0) );
        this.waitingSeries[seriesUID] = seriesUrls;
    }
    this.loadURL(firstSlidesUrls);
}


/**
 * Add new series to existing application.
 * @method addSeries
 * @param {Object} data The data to display.
 */
dwv.Launcher.prototype.addSeries = function(data)
{
    var dicomData = data.dicomElements;
    var seriesId = dicomData.getFromName('SeriesInstanceUID');
    this.apps[seriesId].app.addLayer(data);
};

/**
 * Open gui of some existing application.
 * @method addSeries
 * @param {Object} data The data to display.
 */
dwv.Launcher.prototype.openApp = function(appGui, win)
{
    this.windowsManager.openApp(appGui, win);
    this.activateApp(appGui);
    this.seriesSynchronizer.desynchronize();

    var seriesUID = appGui.app.seriesId;
    var seriesUrls = this.waitingSeries[seriesUID];
    if ( seriesUrls ) {
        this.loadURL(seriesUrls, false);
        this.waitingSeries[seriesUID] = null;
    }
};

/**
 * Activate gui of some existing application.
 * @method addSeries
 * @param {Object} data The data to display.
 */
dwv.Launcher.prototype.activateApp = function(appGui)
{
    this.windowsManager.activateApp(appGui);
    this.activeAppGui = appGui;
    this.onActivateApp();
};


/**
 * Open gui of some existing application.
 * @method addSeries
 * @param {Object} data The data to display.
 */
dwv.Launcher.prototype.onActivateApp = function()
{
    if ( this.toolbox ) {
        this.toolbox.init();
        this.toolbox.display(true);
    }
    if ( this.tagsGui ) {
        this.tagsGui.initialise( this.activeAppGui.app.info );
    }
};

dwv.Launcher.prototype.changeSchema = function(schema)
{
    this.seriesSynchronizer.desynchronize();
    this.windowsManager.changeSchema(schema)
};

dwv.Launcher.prototype.incrementSliceNb = function(sync)
{
    var openedApps = this.openedApps();
    if (sync) {
        this.seriesSynchronizer.syncronize(openedApps);
        this.seriesSynchronizer.incrementSliceNb();
    } else {
        this.activeAppGui.app.incrementSliceNb()
    }
};

dwv.Launcher.prototype.decrementSliceNb = function(sync)
{
    var openedApps = this.openedApps();
    if (sync) {
        this.seriesSynchronizer.syncronize(openedApps);
        this.seriesSynchronizer.decrementSliceNb();
    } else {
        this.activeAppGui.app.decrementSliceNb()
    }
};

dwv.Launcher.prototype.openedApps = function() {
    var openedApps = this.windowsManager.getApps();
    var apps = [];
    for (var k =0; k < openedApps.length; k++) {
        apps.push( openedApps[k].app );
    }
   return apps;
};


/**
 * Handle an error: display it to the user.
 * @method handleError
 * @param {Object} error The error to handle.
 */
dwv.Launcher.prototype.handleError = function(error)
{
    // alert window
    if ( error.name && error.message) {
        console.error(error.name+": "+error.message+".");
    }
    else {
        console.error("Error: "+error+".");
    }
    //log
    if ( error.stack ) {
        console.error(error.stack);
    }
};

/**
 * Get a HTML element associated to the application.
 * @method getElement
 * @param name The name or id to find.
 * @return The found element or null.
 */
dwv.Launcher.prototype.getElement = function (name)
{
    return dwv.gui.getElement(this.containerId, name);
};


/**
 * Get the data loaders.
 * @method getLoaders
 * @return {Object} The loaders.
 */
dwv.Launcher.prototype.getLoaders = function ()
{
    return {
        'file': dwv.io.File,
        'url': dwv.io.Url
    };
};

/**
 * Get the undo stack.
 * @method getUndoStack
 * @return {Object} The undo stack.
 */
dwv.Launcher.prototype.getUndoStack = function ()
{
    return this.undoStack;
};

/**
 * Handle change files event.
 * @method onChangeFiles
 * @param {Object} event The event fired when changing the file field.
 */
dwv.Launcher.prototype.onChangeFiles = function (event)
{
    var files = event.target.files;
    if ( files.length !== 0 ) this.loadImageFiles(files);
};

/**
 * Get the view controller for current application.
 * @method getViewController
 * @return {Object} The controller.
 */
dwv.Launcher.prototype.getViewController = function ()
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getViewController();
    }
};

/**
 * Get the image from active application.
 * @method getImage
 * @return {Image} The associated image.
 */
dwv.Launcher.prototype.getImage = function()
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getImage();
    }
};

/**
 * Set the image to active application.
 * @method setImage
 * @param {Image} img The associated image.
 */
dwv.Launcher.prototype.setImage = function (img)
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.setImage(img);
    }
};

/**
 * Render the current image.
 * @method render
 */
dwv.Launcher.prototype.render = function ()
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.render();
    }
};

/**
 * Get the image data array.
 * @method getImageData
 * @return {Array} The image data array.
 */
dwv.Launcher.prototype.getImageData = function ()
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getImageData();
    }
};

/**
 * Get the number of slices to load.
 * @method getNSlicesToLoad
 * @return {Number} The number of slices to load.
 */
dwv.Launcher.prototype.getNSlicesToLoad = function () {
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getImage();
    }
};

/**
 * Add a translation to the layers.
 * @method stepTranslate
 * @param {Number} tx The step translation along X.
 * @param {Number} ty The step translation along Y.
 */
dwv.Launcher.prototype.stepTranslate = function (tx, ty)
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.stepTranslate(tx, ty);
    }
};

/**
 * Add a step to the layers zoom.
 * @method stepZoom
 * @param {Number} step The zoom step increment. A good step is of 0.1.
 * @param {Number} cx The zoom center X coordinate.
 * @param {Number} cy The zoom center Y coordinate.
 */
dwv.Launcher.prototype.stepZoom = function (step, cx, cy)
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.stepZoom(step, cx, cy);
    }
};

/**
 * Get the app style.
 * @method getStyle
 * @return {Object} The app style.
 */
dwv.Launcher.prototype.getStyle = function ()
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getStyle();
    }
};

/**
 * Handle window/level preset change on active application.
 * @method onChangeWindowLevelPreset
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onChangeWindowLevelPreset = function (event)
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.onChangeWindowLevelPreset.call(event.target);
    }
};

/**
 * Handle colour map change.
 * @method onChangeColourMap
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onChangeColourMap = function (event)
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.onChangeColourMap.call(event.target);
    }
};

/**
 * Handle zoom reset.
 * @method onZoomReset
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onZoomReset = function (/*event*/)
{
    if ( this.activeAppGui ) {
        this.activeAppGui.app.onZoomReset();
    }
};

/**
 * Get the draw stage of active application.
 * @method getDrawStage
 * @return {Object} The draw layer.
 */
dwv.Launcher.prototype.getDrawStage = function ()
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getDrawStage();
    }
};

/**
 * Get the draw layer.
 * @method getDrawLayer
 * @return {Object} The draw layer.
 */
dwv.Launcher.prototype.getDrawLayer = function (k)
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.getDrawLayer(k);
    }
};

/**
 * Add layer mouse and touch listeners.
 * @method addLayerListeners
 */
dwv.Launcher.prototype.addLayerListeners = function (layer)
{
    return this.activeAppGui.app.addLayerListeners(layer);
};

/**
 * Remove layer mouse and touch listeners.
 * @method removeLayerListeners
 */
dwv.Launcher.prototype.removeLayerListeners = function (layer)
{
    return this.activeAppGui.app.removeLayerListeners(layer);
};

/**
 * Handle tool change.
 * @method onChangeTool
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onChangeTool = function (event)
{
    this.toolboxController.setSelectedTool(event.target.value);
};

/**
 * Handle shape change.
 * @method onChangeShape
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onChangeShape = function (event)
{
    this.toolboxController.setSelectedShape(event.target.value);
};

/**
 * Handle filter change.
 * @method onChangeFilter
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onChangeFilter = function (event)
{
    this.toolboxController.setSelectedFilter(event.target.value);
};

/**
 * Handle filter run.
 * @method onRunFilter
 * @param {Object} event The run event.
 */
dwv.Launcher.prototype.onRunFilter = function (/*event*/)
{
    this.toolboxController.runSelectedFilter();
};

/**
 * Handle line colour change.
 * @method onChangeLineColour
 * @param {Object} event The change event.
 */
dwv.Launcher.prototype.onChangeLineColour = function (event)
{
    this.toolboxController.setLineColour(event.target.value);
};

/**
 * Handle min/max slider change.
 * @method onChangeMinMax
 * @param {Object} range The new range of the data.
 */
dwv.Launcher.prototype.onChangeMinMax = function (range)
{
    this.toolboxController.setRange(range);
};

/**
 * Push command in undo stack.
 * @method addCommandToStack
 * @param {Command} pushed command.
 */
dwv.Launcher.prototype.addCommandToStack = function(command)
{
    if ( !this.getUndoStack() ) return;
    this.getUndoStack().add(command);
};


/**
 * General-purpose event handler. Proxy event to active application.
 * @method eventHandler
 * @private
 * @param {Object} event The event to handle.
 */
dwv.Launcher.prototype.eventHandler = function(event)
{
    if ( this.activeAppGui ) {
        return this.activeAppGui.app.handleEvent(event);
    }
};

/**
 * Handle key down event.
 * - CRTL-Z: undo
 * - CRTL-Y: redo
 * Default behavior. Usually used in tools.
 * @method onKeydown
 * @param {Object} event The key down event.
 */
dwv.Launcher.prototype.onKeydown = function (event)
{
    if ( event.keyCode === 90 && event.ctrlKey ) // ctrl-z
    {
        this.getUndoStack().undo();
    }
    else if ( event.keyCode === 89 && event.ctrlKey ) // ctrl-y
    {
        this.getUndoStack().redo();
    }
};