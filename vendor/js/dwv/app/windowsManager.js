// Main DWV namespace.
var dwv = dwv || {};

dwv.WindowsManager = function(launcher) {
    this.windowSchema = null;
    this.apps = {};
    this.launcher = launcher;
    this.element = launcher.getElement('app-container');
};

var SCHEMA = {
  '1x1' : 1,
  '1x2' : 2,
  '1x3' : 3,
  '2x1' : 2,
  '2x2' : 4,
  '2x3' : 6,
  '3x1' : 3,
  '3x2' : 6,
  '3x3' : 9
};

dwv.WindowsManager.prototype.openApp = function( app, win ) {
    if ( !this.windowSchema ) {
        var nextSchema = this.nextSchema();
        this.changeSchema( nextSchema );
    }
    var winId = win || this.getFreeWindow() || 'win-11';
    this.putAppToWindow(app, winId);
    app.controlGui.activate(true);
};

dwv.WindowsManager.prototype.closeApp = function( app ) {
    this.launcher.getElement('app-cache').appendChild( app.appEl );
    app.controlGui.activate(false);
    for ( var k in this.apps ) {
        if ( this.apps[k].getId() == app.getId() ) {
            delete this.apps[k];
        }
    }
};

dwv.WindowsManager.prototype.activateApp = function( app ) {
    for ( var k in this.apps ) {
        if ( this.apps[k].getId() != app.getId() ) {
            this.apps[k].activate(false);
        }
    }
    app.activate( true );
};

dwv.WindowsManager.prototype.getSchema = function() {
    return this.windowSchema;
};

dwv.WindowsManager.prototype.putAppToWindow = function( app, winName ) {
    winName = winName || 'win-11';
    var prevApp = this.apps[winName];
    if ( prevApp ) this.closeApp( prevApp );
    var win = this.getElement( winName );
    win.appendChild( app.appEl );
    app.fitToParentSize();
    this.apps[winName] = app;
};

dwv.WindowsManager.prototype.getFreeWindow = function() {
    var win;
    var cols = this.getCols();
    var rows = this.getRows();
    for (var j = 1; j <= rows; j++) {
        for (var i = 1; i <= cols; i++) {
            win = 'win-' + j + i;
            if ( !this.apps[win] ) return win;
        }
    }
    return false
};

dwv.WindowsManager.prototype.changeSchema = function( schemeId ) {
    this.windowSchema = schemeId;
    this.element.className = this.element.className.split(' ')[0] + ' sch-' + schemeId;
    var win;
    var cols = this.getCols();
    var rows = this.getRows();

    for (var j = 1; j <= 3; j++) {
        for (var i = 1; i <= 3; i++) {
            if (j > rows || i > cols ){
                win = 'win-' + j + i;
                if ( this.apps[win] ) this.closeApp( this.apps[win] );
            }
        }
    }

    for ( appId in this.apps) {
        this.apps[appId].fitToParentSize();
    }
};

dwv.WindowsManager.prototype.getCols = function() {
    if ( !this.getSchema() ) return 1;
    return parseInt(this.getSchema().split('x')[1]);
};

dwv.WindowsManager.prototype.getRows = function() {
    if ( !this.getSchema() ) return 1;
    return parseInt(this.getSchema().split('x')[0]);
};

dwv.WindowsManager.prototype.nextSchema = function( schemeId ) {
    if (!this.windowSchema) return '1x1';
    switch ( this.windowSchema ) {
        case '1x1':
            return '1x2';
        case '1x2':
            return '1x3';
        case '1x3':
            return '2x2';
        case '2x1':
            return '3x1';
        case '2x2':
            return '2x3';
        case '2x3':
            return '3x3';
        case '3x1':
            return '3x2';
        case '3x2':
            return '3x3';
        default:
            return false;
    }
};

dwv.WindowsManager.prototype.getElement = function(name) {
    return this.element.getElementsByClassName(name)[0];
};

dwv.WindowsManager.prototype.getApps = function() {
    var apps = [];
    for ( var k in this.apps ) {
        if (this.apps[k]) apps.push( this.apps[k] );
    }
    return apps;
};
