// Main DWV namespace.
var dwv = dwv || {};

dwv.SeriesSynchronizer = function() {
    this.position = 0;
    this.maxSlices = 0;
    this.syncronized = false;
    this.apps = [];
};

dwv.SeriesSynchronizer.prototype.syncronize = function(apps) {
    if (this.syncronized) return;
    this.apps = apps;
    this.maxSlices = 0;
    for (var k =0; k < this.apps.length; k++) {
        var app = this.apps[k];
        app.setSliceNb(0);
        if ( app.getTotalSlices() > this.maxSlices ) {
            this.maxSlices = app.getTotalSlices();
            this.position = app.getCurrentSlice();
        }
    }
    this.syncronized = true;
};

dwv.SeriesSynchronizer.prototype.stepMul = function() {
   return 1 / ( this.maxSlices );
};

dwv.SeriesSynchronizer.prototype.incrementSliceNb = function() {
    if ( this.position == this.maxSlices - 1 ) return false;
    this.position += 1;
    this.sliceToCurrentPosition();
};

dwv.SeriesSynchronizer.prototype.decrementSliceNb = function() {
    if ( this.position == 0 ) return;
    this.position -= 1;
    this.sliceToCurrentPosition();
};

dwv.SeriesSynchronizer.prototype.sliceToCurrentPosition = function() {
    var mul = this.stepMul() * (this.position);
    for (var k =0; k < this.apps.length; k++) {
        var app = this.apps[k];
        if ( this.maxSlices == app.getTotalSlices() ) {
            app.setSliceNb(this.position);
        }
        else {
            var appSlice = Math.floor( Math.round( app.getTotalSlices() * mul * 100) / 100 );
            if ( appSlice != app.getCurrentSlice() ) {
                app.setSliceNb(appSlice);
            }
        }
    }
};

dwv.SeriesSynchronizer.prototype.desynchronize = function() {
    this.syncronized = false;
};