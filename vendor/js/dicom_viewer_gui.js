/*
 * Application GUI.
 */
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

dwv.gui.App = dwv.gui.base.App;

dwv.gui.App.prototype.fitToParentSize = function() {
  return this.app.fitToSize({
    width: $(this.appEl).innerWidth(),
    height: $(this.appEl).innerHeight()
  });
};

dwv.gui.AppControl = dwv.gui.base.AppControl;

dwv.tool.colourMaps = {
  "plain": dwv.image.lut.plain,
  "invplain": dwv.image.lut.invPlain,
  "rainbow": dwv.image.lut.rainbow,
  "hot": dwv.image.lut.hot,
  "hot iron": dwv.image.lut.hot_iron,
  "pet": dwv.image.lut.pet,
  "hot metal blue": dwv.image.lut.hot_metal_blue,
  "pet 20 step": dwv.image.lut.pet_20step
};

dwv.tool.defaultpresets = {};

dwv.tool.defaultpresets.CT = {
  "mediastinum": {
    "center": 40,
    "width": 400
  },
  "lung": {
    "center": -500,
    "width": 1500
  },
  "bone": {
    "center": 500,
    "width": 2000
  },
  "brain": {
    "center": 40,
    "width": 80
  },
  "head": {
    "center": 90,
    "width": 350
  }
};

dwv.onApplicationStarted = function(launcher) {
  return $('.app-container .win').droppable({
    scope: 'dwv-app',
    drop: function(e, ui) {
      var win;
      win = $(e.target).data('index');
      return ui.draggable.data('gui').open(win);
    }
  });
};

dwv.onAppControlCreate = function(appControlGui) {
  return $(appControlGui.el).data('gui', appControlGui).draggable({
    helper: 'clone',
    appendTo: 'body',
    scope: 'dwv-app'
  });
};

dwv.gui.Base = (function() {
  Base.prototype.app = null;

  function Base(app) {
    this.app = app;
  }

  Base.prototype.display = function(bool) {
    if (bool) {
      return this.$el.removeClass('hidden');
    } else {
      return this.$el.addClass('hidden');
    }
  };

  Base.prototype.$ = function(selector) {
    return this.$el.find(selector);
  };

  Base.prototype.event = function(data) {
    return {
      target: {
        value: data
      }
    };
  };

  Base.prototype.initialise = function() {};

  return Base;

})();

dwv.gui.getElement = dwv.gui.base.getElement;

dwv.gui.refreshElement = dwv.gui.base.refreshElement;

dwv.gui.DicomTags = dwv.gui.base.DicomTags;

dwv.gui.Loadbox = dwv.gui.base.Loadbox;

dwv.gui.FileLoad = dwv.gui.base.FileLoad;

dwv.gui.UrlLoad = dwv.gui.base.UrlLoad;

dwv.gui.Slider = dwv.gui.base.Slider;

dwv.gui.Toolbox = (function(superClass) {
  extend(Toolbox, superClass);

  function Toolbox() {
    return Toolbox.__super__.constructor.apply(this, arguments);
  }

  Toolbox.prototype.toolIcons = {
    'Scroll': 'glyphicon-resize-vertical',
    'Window/Level': 'glyphicon-adjust',
    'Zoom/Pan': 'glyphicon-search',
    'Draw': 'glyphicon-pencil',
    'Filter': 'glyphicon-signal',
    'Livewire': 'glyphicon-flash'
  };

  Toolbox.prototype.initEvents = function() {
    return this.$el.on('click', '.btn', this.onChangeTool.bind(this));
  };

  Toolbox.prototype.setup = function(list) {
    this.$el = $('<div>').addClass('btn-group');
    $.each(list, (function(_this) {
      return function(tool) {
        return _this.$el.append(_this.renderBtn(tool));
      };
    })(this));
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    this.app.getElement("app-toolbox").appendChild(this.renderProgressBar());

    this.$('.btn:first').addClass('active');
    return this.initEvents();
  };

  Toolbox.prototype.onChangeTool = function(e) {
    var $btn;
    this.$('.btn').removeClass('active');
    $btn = $(e.currentTarget);
    $btn.addClass('active');
    return this.app.onChangeTool(this.event($btn.data('tool')));
  };

  Toolbox.prototype.initialise = function(displays) {
    this.$('.btn').removeClass('active');
    return this.$('.btn:first').addClass('active');
  };

  Toolbox.prototype.toolNameToClass = function(name) {
    return name.toLowerCase().replace('/', '-');
  };

  Toolbox.prototype.renderBtn = function(tool) {
    return "<button class='btn btn-default btn-" + (this.toolNameToClass(tool)) + "' data-tool='" + tool + "' title='" + (dwv.utils.capitaliseFirstLetter(tool)) + "'> <i class='glyphicon " + this.toolIcons[tool] + "'></i> </button>";
  };

  Toolbox.prototype.renderProgressBar = function() {
    return $("<div class='dwv-progress'> <div class='dwv-progress-bar'></div> </div>").get(0);
  };

  return Toolbox;

})(dwv.gui.Base);

dwv.gui.WindowLevel = (function(superClass) {
  extend(WindowLevel, superClass);

  function WindowLevel() {
    return WindowLevel.__super__.constructor.apply(this, arguments);
  }

  WindowLevel.prototype.level = null;

  WindowLevel.prototype.colorMap = null;

  WindowLevel.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group group-window-level hidden');
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    return this.initEvents();
  };

  WindowLevel.prototype.initEvents = function() {
    this.$el.on('click', '.group-wl .btn-item', this.onChangeWindowLevelPreset.bind(this));
    return this.$el.on('click', '.group-cm .btn-item', this.onChangeColourMap.bind(this));
  };

  WindowLevel.prototype.onChangeWindowLevelPreset = function(e) {
    this.level = $(e.currentTarget).data('value');
    return this.app.onChangeWindowLevelPreset(this.event(this.level));
  };

  WindowLevel.prototype.onChangeColourMap = function(e) {
    this.colorMap = $(e.currentTarget).data('value');
    return this.app.onChangeColourMap(this.event(this.colorMap));
  };

  WindowLevel.prototype.renderWlGroup = function() {
    this.$el.append(dwv.html.createDropdown('group-wl', this.app.getViewController().getPresets(), this.level));

  };

  WindowLevel.prototype.renderCmGroup = function() {
    this.$el.append(dwv.html.createDropdown('group-cm', dwv.tool.colourMaps, this.colorMap));

  };

  WindowLevel.prototype.initialise = function() {
    this.$el.empty();
    this.renderWlGroup();
    return this.renderCmGroup();
  };

  return WindowLevel;

})(dwv.gui.Base);

dwv.gui.ZoomAndPan = (function(superClass) {
  extend(ZoomAndPan, superClass);

  function ZoomAndPan() {
    return ZoomAndPan.__super__.constructor.apply(this, arguments);
  }

  ZoomAndPan.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group group-zoom-pan hidden');
    this.$el.append("<button class='btn btn-default btn-reset'>Reset</button>");
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    return this.initEvents();
  };

  ZoomAndPan.prototype.initEvents = function() {
    return this.$el.on('click', '.btn-reset', (function(_this) {
      return function() {
        return _this.app.onZoomReset();
      };
    })(this));
  };

  return ZoomAndPan;

})(dwv.gui.Base);



dwv.gui.Magnifier = (function(superClass) {
  extend(Magnifier, superClass);

  function Magnifier() {
    return Magnifier.__super__.constructor.apply(this, arguments);
  }

  Magnifier.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group group-zoom-pan hidden');
    this.$el.append("<button class='btn btn-default btn-reset'>Reset</button>");
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    return this.initEvents();
  };

  Magnifier.prototype.initEvents = function() {
    return this.$el.on('click', '.btn-reset', (function(_this) {
      return function() {
        return _this.app.onZoomReset();
      };
    })(this));
  };

  return Magnifier;

})(dwv.gui.Base);



dwv.gui.Draw = (function(superClass) {
  extend(Draw, superClass);

  function Draw() {
    return Draw.__super__.constructor.apply(this, arguments);
  }

  Draw.prototype.colours = ["Yellow", "Red", "White", "Green", "Blue", "Lime", "Fuchsia", "Black"];

  Draw.prototype.setup = function(shapeList) {
    this.$el = $('<div>').addClass('btn-group group-draw hidden');
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    this.$el.append(dwv.html.createDropdown('group-shape', shapeList));
    this.$el.append(dwv.html.createDropdownColor('group-colour', this.colours));

    return this.initEvents();
  };

  Draw.prototype.getColours = function() {
    return this.colours;
  };

  Draw.prototype.initEvents = function() {
    this.$el.on('click', '.group-shape .btn-item', (function(_this) {
      return function(e) {
        return _this.app.onChangeShape(_this.event($(e.currentTarget).data('value')));
      };
    })(this));
    return this.$el.on('click', '.group-colour .color-item', (function(_this) {
      return function(e) {
        return _this.app.onChangeLineColour(_this.event($(e.currentTarget).data('value')));
      };
    })(this));
  };

  Draw.prototype.initialise = function() {};

  return Draw;

})(dwv.gui.Base);

dwv.gui.Livewire = (function(superClass) {
  extend(Livewire, superClass);

  function Livewire() {
    return Livewire.__super__.constructor.apply(this, arguments);
  }

  Livewire.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group livewire hidden');
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    this.$el.append(dwv.html.createDropdownColor('group-colour', this.colours));

    return this.initEvents();
  };

  Livewire.prototype.initEvents = function() {
    return this.$el.on('click', '.group-colour .color-item', (function(_this) {
      return function(e) {
        return _this.app.onChangeLineColour(_this.event($(e.currentTarget).data('value')));
      };
    })(this));
  };

  return Livewire;

})(dwv.gui.Draw);

dwv.gui.Filter = (function(superClass) {
  extend(Filter, superClass);

  function Filter() {
    return Filter.__super__.constructor.apply(this, arguments);
  }

  Filter.prototype.setup = function(list) {
    this.$el = $('<div>').addClass('btn-group group-filter hidden');
    this.$el.append(dwv.html.createDropdown('group-filters', list));

    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    return this.initEvents();
  };

  Filter.prototype.initEvents = function() {
    return this.$el.on('click', '.group-filters .btn-item', (function(_this) {
      return function(e) {
        return _this.app.onChangeFilter(_this.event($(e.currentTarget).data('value')));
      };
    })(this));
  };

  Filter.prototype.initialise = function() {};

  return Filter;

})(dwv.gui.Base);

dwv.gui.Threshold = (function(superClass) {
  extend(Threshold, superClass);

  function Threshold() {
    return Threshold.__super__.constructor.apply(this, arguments);
  }

  Threshold.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group threshold-filter hidden');
    return this.app.getElement("group-filter").appendChild(this.$el.get(0));
  };

  Threshold.prototype.initialise = function() {
    var $slider, max, min, timer;
    min = this.app.getImage().getDataRange().min;
    max = this.app.getImage().getDataRange().max;
    $slider = $('<input>');
    this.$el.empty().append($slider);
    timer = null;
    return $slider.slider({
      range: true,
      min: min,
      max: max,
      value: [min, max]
    }).on('slide', (function(_this) {
      return function(event) {
        if (timer) {
          clearTimeout(timer);
        }
        return timer = setTimeout((function() {
          return _this.app.onChangeMinMax({
            'min': event.value[0],
            'max': event.value[1]
          });
        }), 100);
      };
    })(this));
  };

  return Threshold;

})(dwv.gui.Base);

dwv.gui.Sharpen = (function(superClass) {
  extend(Sharpen, superClass);

  function Sharpen() {
    return Sharpen.__super__.constructor.apply(this, arguments);
  }

  Sharpen.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group sharpen-filter hidden');
    this.app.getElement("group-filter").appendChild(this.$el.get(0));
    this.$el.append("<button class='btn btn-default'>Apply</button>");
    return this.initEvents();
  };

  Sharpen.prototype.initEvents = function() {
    return this.$el.on('click', 'button', (function(_this) {
      return function() {
        return _this.app.onRunFilter();
      };
    })(this));
  };

  return Sharpen;

})(dwv.gui.Base);

dwv.gui.Sobel = (function(superClass) {
  extend(Sobel, superClass);

  function Sobel() {
    return Sobel.__super__.constructor.apply(this, arguments);
  }

  Sobel.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group sobel-filter hidden');
    this.app.getElement("group-filter").appendChild(this.$el.get(0));
    this.$el.append("<button class='btn btn-default'>Apply</button>");
    return this.initEvents();
  };

  Sobel.prototype.initEvents = function() {
    return this.$el.on('click', 'button', (function(_this) {
      return function() {
        return _this.app.onRunFilter();
      };
    })(this));
  };

  return Sobel;

})(dwv.gui.Base);

dwv.gui.Undo = (function(superClass) {
  extend(Undo, superClass);

  function Undo() {
    return Undo.__super__.constructor.apply(this, arguments);
  }

  Undo.prototype.commandsCount = 0;

  Undo.prototype.commandIndex = 0;

  Undo.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group group-undo');
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    this.$el.append("<button class='btn btn-default btn-undo' title='Undo'><i class='glyphicon glyphicon-chevron-left'></i></button>");
    this.$el.append("<button class='btn btn-default btn-redo' title='Redo'><i class='glyphicon glyphicon-chevron-right'></i></button>");

    this.initEvents();
    this.resetButtons();
    return this.display(false);
  };

  Undo.prototype.initEvents = function() {
    this.$el.on('click', '.btn-undo', (function(_this) {
      return function() {
        return _this.undo();
      };
    })(this));
    return this.$el.on('click', '.btn-redo', (function(_this) {
      return function() {
        return _this.redo();
      };
    })(this));
  };

  Undo.prototype.initialise = function() {
    return this.commandIndex = 0;
  };

  Undo.prototype.addCommandToUndoHtml = function(commandName) {
    this.commandIndex++;
    this.commandsCount = this.commandIndex;
    this.resetButtons();
    return this.display(true);
  };

  Undo.prototype.enableInUndoHtml = function(enable) {
    if (enable) {
      this.commandIndex++;
    } else {
      this.commandIndex--;
    }
    return this.resetButtons();
  };

  Undo.prototype.resetButtons = function() {
    if (this.commandIndex > 0) {
      this.$('.btn-undo').attr('disabled', false);
    } else {
      this.$('.btn-undo').attr('disabled', true);
    }
    if (this.commandIndex < this.commandsCount) {
      return this.$('.btn-redo').attr('disabled', false);
    } else {
      return this.$('.btn-redo').attr('disabled', true);
    }
  };

  Undo.prototype.undo = function() {
    if (this.commandIndex > 0) {
      return this.app.getUndoStack().undo();
    }
  };

  Undo.prototype.redo = function() {
    if (this.commandIndex < this.commandsCount) {
      return this.app.getUndoStack().redo();
    }
  };

  return Undo;

})(dwv.gui.Base);

dwv.gui.Loadbox = (function(superClass) {
  extend(Loadbox, superClass);

  function Loadbox() {
    return Loadbox.__super__.constructor.apply(this, arguments);
  }

  Loadbox.prototype.setup = function() {
    if (this.app.getElement("group-undo")) {
      this.$el = $(this.app.getElement("group-undo"));
    } else {
      this.$el = $('<div>').addClass('btn-group group-undo');
      $(this.app.getElement("app-toolbox")).prepend(this.$el.get(0));
      this.$el.append("<button class='btn btn-default btn-open' title='Open'><i class='glyphicon glyphicon-folder-open'></i></button>");
      this.$el.append("<input class='files' type='file' mozdirectory='true' directory='true' webkitdirectory='true' multiple style='display: none'>");

    }
    return this.initEvents();
  };

  Loadbox.prototype.initEvents = function() {
    this.$el.on('click', '.btn-open', this.openFileDialog.bind(this));
    return this.$el.on('change', '.files', this.loadFiles.bind(this));
  };

  Loadbox.prototype.openFileDialog = function() {
    return this.$('input.files').click();
  };

  Loadbox.prototype.loadFiles = function(event) {
    return this.app.onChangeFiles(event);
  };

  return Loadbox;

})(dwv.gui.Base);

dwv.gui.FileLoad = dwv.gui.base.FileLoad;

dwv.gui.UrlLoad = dwv.gui.base.UrlLoad;

dwv.gui.Layout = (function(superClass) {
  extend(Layout, superClass);

  function Layout() {
    return Layout.__super__.constructor.apply(this, arguments);
  }

  Layout.prototype.init = function() {};

  Layout.prototype.setup = function() {
    var $layoutBtn, $tools;
    $tools = $(this.app.getElement("app-toolbox"));
    this.$el = $('<div>').addClass('btn-group group-layout');
    $layoutBtn = $('<button>').addClass('btn btn-default btn-dropdown').append($('<i>').addClass('glyphicon glyphicon-th'));
    this.$el.append($layoutBtn).append(this.buildSchemaElement());
    $tools.append(this.$el);
    return this.initEvents();
  };

  Layout.prototype.display = function(bool) {
    if (bool) {
      this.$('.btn-dropdown').addClass('active');
      return this.$el.addClass('open');
    } else {
      this.$('.btn-dropdown').removeClass('active');
      return this.$el.removeClass('open');
    }
  };

  Layout.prototype.initEvents = function() {
    this.$el.on('mouseenter', '.btn-schema', this.activateCells.bind(this));
    this.$el.on('click', '.btn-schema', this.changeSchema.bind(this));
    return this.$el.on('click', '.btn-dropdown', this.display.bind(this));
  };

  Layout.prototype.activateCells = function(event) {
    var cols, i, j, l, ref, results, rows, schema;
    schema = $(event.currentTarget).data('schema');
    rows = parseInt(schema.split('x')[0]);
    cols = parseInt(schema.split('x')[1]);
    this.$(".btn-schema").removeClass('active');
    results = [];
    for (j = l = 1, ref = rows; 1 <= ref ? l <= ref : l >= ref; j = 1 <= ref ? ++l : --l) {
      results.push((function() {
        var m, ref1, results1;
        results1 = [];
        for (i = m = 1, ref1 = cols; 1 <= ref1 ? m <= ref1 : m >= ref1; i = 1 <= ref1 ? ++m : --m) {
          results1.push(this.$(".btn-schema." + j + "x" + i).addClass('active'));
        }
        return results1;
      }).call(this));
    }
    return results;
  };

  Layout.prototype.changeSchema = function(event) {
    var schema;
    schema = $(event.currentTarget).data('schema');
    this.app.changeSchema(schema);
    return this.display(false);
  };

  Layout.prototype.buildSchemaElement = function() {
    var $cells, i, k, l, m;
    $cells = $('<div>').addClass('schema-select dropdown-menu');
    for (k = l = 1; l <= 3; k = ++l) {
      for (i = m = 1; m <= 3; i = ++m) {
        $cells.append($('<button>').addClass("btn-schema " + k + "x" + i).data('schema', k + "x" + i));
      }
    }
    return $cells;
  };

  return Layout;

})(dwv.gui.Base);

dwv.gui.Scroll = (function(superClass) {
  extend(Scroll, superClass);

  Scroll.prototype.syncTool = null;

  function Scroll(app, syncTool) {
    this.syncTool = syncTool;
    this.app = app;
  }

  Scroll.prototype.setup = function() {
    this.$el = $('<div>').addClass('btn-group group-scroll hidden');
    this.$el.append("<button class='btn btn-default btn-scroll-sync' title='Synchronize Series'><i class='glyphicon glyphicon-tasks'></i></button>");
    this.app.getElement("app-toolbox").appendChild(this.$el.get(0));
    return this.initEvents();
  };

  Scroll.prototype.initEvents = function() {
    return this.$el.on('click', '.btn-scroll-sync', (function(_this) {
      return function() {
        return _this.toggleSync();
      };
    })(this));
  };

  Scroll.prototype.initialise = function() {};

  Scroll.prototype.toggleSync = function() {
    this.$('.btn-scroll-sync').toggleClass('active');
    return this.syncTool.setSync(this.$('.btn-scroll-sync').hasClass('active'));
  };

  return Scroll;

})(dwv.gui.Base);

dwv.gui.appendHelpHtml = dwv.gui.base.appendHelpHtml;

dwv.gui.appendVersionHtml = dwv.gui.base.appendVersionHtml;

dwv.html.createDropdown = function(cls, list, selected) {
  var firstValue, group, itemBuilder, menu;
  group = $('<div>').addClass("btn-group " + cls);
  if (!list) {
    return;
  }
  firstValue = selected || ($.isArray(list) ? list[0] : $.map(list, function(_, item) {
    return item;
  })[0]);
  group.append("<button class='btn btn-default dropdown-toggle' data-toggle='dropdown'> <span class='caption'>" + (dwv.utils.capitaliseFirstLetter(firstValue)) + "</span> <span class='caret'></span> </button>");
  itemBuilder = function(item) {
    return "<li><a class='btn-item' data-value='" + item + "'>" + (dwv.utils.capitaliseFirstLetter(item)) + "</a></li>";
  };
  menu = $('<ul>').addClass('dropdown-menu').prop('role', 'menu');
  group.append(menu);
  if ($.isArray(list)) {
    $.each(list, (function(_this) {
      return function(_, item) {
        return menu.append(itemBuilder(item));
      };
    })(this));
  } else {
    $.each(list, (function(_this) {
      return function(item) {
        return menu.append(itemBuilder(item));
      };
    })(this));
  }
  group.on('click', '.btn-item', function(e) {
    var $btn;
    $btn = $(e.currentTarget);
    group.find('.btn-item').removeClass('active');
    $btn.addClass('active');
    return group.find('.caption').text(dwv.utils.capitaliseFirstLetter($btn.data('value')));
  });
  return group;
};

dwv.html.createDropdownColor = function(cls, list, selected) {
  var firstValue, group, itemBuilder, menu;
  group = $('<div>').addClass("btn-group " + cls);
  if (!list) {
    return;
  }
  firstValue = selected || list[0];
  group.append("<button class='btn btn-default dropdown-toggle' data-toggle='dropdown'> <span class='caption color-item' style='background-color: " + firstValue + "'></span> <span class='caret'></span> </button>");
  itemBuilder = function(item) {
    return "<li><div class='color-item' data-value='" + item + "' style='background-color: " + item + "'></div></li>";
  };
  menu = $('<ul>').addClass('dropdown-menu').prop('role', 'menu');
  group.append(menu);
  $.each(list, (function(_this) {
    return function(_, item) {
      return menu.append(itemBuilder(item));
    };
  })(this));
  group.on('click', '.color-item', function(e) {
    var $btn;
    $btn = $(e.currentTarget);
    group.find('.color-item').removeClass('active');
    $btn.addClass('active');
    return group.find('.caption').css('background-color', $btn.data('value'));
  });
  return group;
};

dwv.gui.displayProgress = function(percent) {
  $('.dwv-progress-bar').css({
    width: percent + "%"
  }).text(percent + "%");
  if (percent === 100) {
    setTimeout((function() {
      return $('.dwv-progress').hide();
    }), 2000);
  }
  if (percent === 0) {
    return $('.dwv-progress').show();
  }
};
 console.dir(dwv);
// ---
// generated by coffee-script 1.9.2